import s from "./Button.module.scss";

function Button({ addToCart }) {
  return (
    <button className={s.mainButton} onClick={addToCart}>
      ADD TO CART
    </button>
  );
}

export default Button;
